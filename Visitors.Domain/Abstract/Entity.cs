﻿using System;
using Visitors.Domain.Enums;
using static Visitors.Core.DateTimeProvider;

namespace Visitors.Domain.Abstract
{
    public abstract class Entity : IEntity
    {
        public Guid Id { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public EntityState State { get; set; }

        public Entity()
        {
            CreatedAt = GetCurrentDate();
            ModifiedAt = GetCurrentDate();
            State = EntityState.Active;
        }
    }
}

