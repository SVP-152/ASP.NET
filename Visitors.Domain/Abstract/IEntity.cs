﻿using System;
using Visitors.Domain.Enums;

namespace Visitors.Domain.Abstract
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset ModifiedAt { get; set; }
        EntityState State { get; set; }
    }
}
