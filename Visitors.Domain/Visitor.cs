﻿using System.Collections.Generic;

namespace Visitors.Domain
{
    /// <summary>
    /// представляет посетителя
    /// </summary>
    public class Visitor : Human
    {
        public string Address { get; set; }
        public ICollection<Visit> Visits { get; set; }
    }
}
