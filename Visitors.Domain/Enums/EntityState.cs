﻿namespace Visitors.Domain.Enums
{
    public enum EntityState : byte
    {
        Active = 0,
        Deleted = 1
    }
}
