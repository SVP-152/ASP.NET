﻿using Visitors.Data.Infrastructure;
using Visitors.Domain;

namespace Visitors.Data.Repositories
{
    public interface IVisitRepository : IBaseRepository<Visit>
    {
    }
}
