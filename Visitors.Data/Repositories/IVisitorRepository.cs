﻿using Visitors.Data.Infrastructure;
using Visitors.Domain;

namespace Visitors.Data.Repositories
{
    public interface IVisitorRepository :IBaseRepository<Visitor>
    {
    }
}
