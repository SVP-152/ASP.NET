﻿using Visitors.Data.Infrastructure;
using Visitors.Domain;

namespace Visitors.Data.Repositories
{
    public class VisitorRepository : BaseRepository<Visitor>, IVisitorRepository
    {
        public VisitorRepository(DataContext context) : base(context)
        {

        }

    }
}
