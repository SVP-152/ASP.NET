﻿using Visitors.Data.Infrastructure;
using Visitors.Domain;

namespace Visitors.Data.Repositories
{
    public class VisitRepository : BaseRepository<Visit>, IVisitRepository
    {
        public VisitRepository(DataContext context) : base(context)
        {
        }
    }
}
