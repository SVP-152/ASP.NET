﻿using System.ComponentModel.DataAnnotations;

namespace Visitors.Web.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
