﻿using AutoMapper;
using Visitors.Domain;
using Visitors.Models;
using Visitors.Models.PeopleViewModels;
using Visitors.Models.VisitorsViewModels;
using Visitors.Models.VisitsViewModels;

namespace Visitors.Web.Configuration
{
    public static class AutomapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Person, CreatePersonViewModel>().ReverseMap();
                cfg.CreateMap<Person, DropDownRecord>()
                    .ForMember(dest => dest.Value, src => src.MapFrom(x => x.Id))
                    .ForMember(dest => dest.ValueText, src => src.MapFrom(x => $"{x.LastName} {x.FirstName[0]}."));
                cfg.CreateMap<Person, PersonViewModel>()
                    .ForMember(dest => dest.FullName, src => src.MapFrom(x => $"{x.LastName} {x.FirstName[0]}."))
                    .ForMember(dest => dest.TotalVisits, src => src.MapFrom(x => x.Visits.Count));
                cfg.CreateMap<Person, PersonDetailViewModel>()
                    .ForMember(dest => dest.Gender, src => src.MapFrom(x => x.Gender.ToString()))
                    .ForMember(dest => dest.Visits, src => src.MapFrom(x => x.Visits));

                cfg.CreateMap<Visitor, CreateVisitorViewModel>().ReverseMap();
                cfg.CreateMap<Visitor, VisitorDetailViewModel>()
                    .ForMember(dest => dest.Gender, src => src.MapFrom(x => x.Gender.ToString()))
                    .ForMember(dest => dest.Visits, src => src.MapFrom(x => x.Visits));
                cfg.CreateMap<Visitor, VisitorViewModel>()
                    .ForMember(dest => dest.FullName, src => src.MapFrom(x => $"{x.LastName} {x.FirstName}."))
                    .ForMember(dest => dest.TotalVisits, src => src.MapFrom(x => x.Visits.Count));
                cfg.CreateMap<Visitor, DropDownRecord>()
                    .ForMember(dest => dest.ValueText, src => src.MapFrom(x => $"{x.LastName} {x.FirstName[0]}."))
                    .ForMember(dest => dest.Value, src => src.MapFrom(x => x.Id.ToString()));

                cfg.CreateMap<Visit, VisitViewModel>()
                    .ForMember(dest => dest.VisitedPerson,
                        src => src.MapFrom(x => $"{x.VisitedPerson.LastName} {x.VisitedPerson.FirstName}"))
                    .ForMember(dest => dest.VisitorPerson,
                        src => src.MapFrom(x => $"{x.VisitorPerson.LastName} {x.VisitorPerson.FirstName}"));

                cfg.CreateMap<CreateVisitViewModel, Visit>()
                    .ForMember(dest => dest.VisitedPersonId, src => src.MapFrom(x => x.PersonId))
                    .ForMember(dest => dest.VisitorPersonId, src => src.MapFrom(x => x.VisitorId));
            });
        }
    }
}
