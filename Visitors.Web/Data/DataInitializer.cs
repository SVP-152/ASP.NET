﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Visitors.Data;
using Visitors.Domain;
using Visitors.Domain.Enums;

namespace Visitors.Web.Data
{
    public static class DataInitializer
    {
        public static void SeedTestData(this IApplicationBuilder builder)
        {
            DataContext context = (DataContext)builder.ApplicationServices.GetService(typeof(DataContext));
            List<Visitor> visitors = new List<Visitor>
            {
                new Visitor
                {
                    FirstName = "Erkin",
                    LastName = "Akhmetzhanov",
                    Gender = Gender.Male,
                    Address = "Orinbor 33",
                    Phone = "454545453",
                    IIN = "121212121212"
                },
                new Visitor
                {
                 FirstName   =  "Rustam",
                 LastName =  "Sadvakasov",
                 Address = "Address",
                 Phone = "1111111111",
                 Gender =  Gender.Male,
                 IIN = "321321321321"
                },
                new Visitor
                {
                    FirstName = "Marat",
                    LastName = "Sadykov",
                    Address = "Address",
                    Phone = "111122233",
                    Gender = Gender.Male,
                    IIN = "111222333444"
                }

            };

            for (int i = 1; i < 50; i++)
            {
                visitors.Add(new Visitor
                {
                    FirstName = i.ToString(),
                    LastName = i.ToString(),
                    Address = i.ToString(),
                    Phone = i.ToString(),
                    Gender = i % 4 == 0 ? Gender.Male : i % 10 == 0 ? Gender.NotSet : Gender.Female,
                    IIN = i.ToString(),
                });
            }

            List<Person> persons = new List<Person>
            {
                new Person()
                {
                    FirstName = "Vladislav",
                    LastName = "Shevtsov",
                    Phone = "87078433794",
                    Gender = Gender.Male,
                    IIN = "123456789012",
                },
                new Person()
                {
                    FirstName = "Nikolay",
                    LastName = "Bazankov",
                    Phone = "123456789",
                    Gender = Gender.Male,
                    IIN = "123456789100"
                },

               new Person()
               {
                   FirstName = "Timur",
                   LastName = "Zakirov",
                   Phone = "77848888",
                   Gender = Gender.Male,
                   IIN = "980819300360"
               },
               new Person()
               {
                   FirstName = "Yelena",
                   LastName = "Pak-Umirzakova",
                   Phone = "7015535999",
                   Gender = Gender.Female,
                   IIN = "851009450010"
               }
            };
            if (!context.Visitors.Any())
            {
                context.Visitors.AddRange(visitors);
            }
            if (!context.People.Any())
            {
                context.People.AddRange(persons);
            }
            context.SaveChanges();
        }
    }
}
