﻿using System.Threading.Tasks;

namespace Visitors.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
