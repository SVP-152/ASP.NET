﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Visitors.Domain;
using Visitors.Domain.Enums;
using Visitors.Models.PeopleViewModels;
using Visitors.Services;
using X.PagedList;

namespace Visitors.Web.Controllers
{
    public class PeopleController : Controller
    {
        private readonly PeopleService _peopleService;

        public PeopleController(PeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        // GET: Visitors
        public async Task<IActionResult> Index(int page = 1)
        {
            page = page < 1
                ? 1
                : page;
            var query = await _peopleService
                .GetActiveAsync<PersonViewModel>();
            var pagedList = query
                .ToPagedList(page, Constants.ItemsPerPage);
            return View(pagedList);
        }

        // GET: Visitors/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }
            var person = await _peopleService.GetAsync(id, x => x.Visits);
            if (person == null)
            {
                return NotFound();
            }
            var detailPerson = Mapper.Map<PersonDetailViewModel>(person);
            return View(detailPerson);
        }

        // GET: Visitors/Create
        public IActionResult Create()
        {
            FillGenderList();
            return View();
        }

        // POST: Visitors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreatePersonViewModel model)
        {
            if (!ModelState.IsValid)
            {
                FillGenderList();
                return View(model);
            }
            try
            {
                model.Id = Guid.NewGuid();
                var person = Mapper.Map<Person>(model);
                Person firstPerson = await _peopleService.GetByIinAsync(model.IIN);
                if (firstPerson == null)
                {
                    await _peopleService.AddAsync(person);
                    await _peopleService.SaveAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    FillGenderList();
                    ModelState.AddModelError("Iin", "Посещаемый с таким ИИН уже существует");
                    return View(model);
                }
            }
            catch (Exception e)
            {
                FillGenderList();
                ModelState.AddModelError("", e.Message);
                return View(model);
            }
        }

        // GET: Visitors/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            var dbEntity = await _peopleService.GetAsync(id);
            if (dbEntity != null)
            {
                await _peopleService.DeleteAsync(dbEntity);
                await _peopleService.SaveAsync();
                return RedirectToAction("Index");
            }
            else
            {   //передать сообщение
                ViewData["Message"] = "Нет записи";
                ViewBag.Message = "Нет записи";
                return RedirectToAction("Index");
            }
        }

        private void FillGenderList()
        {
            Dictionary<string, string> selectDictionary = new Dictionary<string, string>();

            Type enumType = typeof(Gender);
            MemberInfo[] members = enumType.GetMembers();
            foreach (var memberInfo in members)
            {
                if (memberInfo.GetCustomAttribute(typeof(DisplayAttribute)) != null)
                {
                    selectDictionary.Add(memberInfo.Name,
                        ((DisplayAttribute)memberInfo.GetCustomAttribute(typeof(DisplayAttribute))).Name);
                }
            }

            ViewData.Add("GenderList", new SelectList(selectDictionary, "Key", "Value"));
        }
    }
}

