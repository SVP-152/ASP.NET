﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Visitors.Data.Infrastructure;
using Visitors.Domain;
using Visitors.Domain.Abstract;
using Visitors.Models;

namespace Visitors.Services
{
    public class SuperBaseService<T> : BaseService<T>, ISuperBaseService<T> where T : Human, IEntity
    {
        public SuperBaseService(IBaseRepository<T> repository) : base(repository)
        {

        }

        public async Task DeleteAsync(T entity)
        {
            await Repository.DeleteAsync(entity);
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await Repository.GetAsync(id);
        }

        public async Task<T> GetAsync(Guid id, params Expression<Func<T, object>>[] includProperties)
        {
            return await Repository.GetAsync(id, includProperties);
        }

        public async Task<T> GetByIinAsync(string iin)
        {
            return await Repository.GetAsync(x => x.IIN == iin.Trim());
        }

        public async Task<IQueryable<DropDownRecord>> GetDropDownRecordsAsync()
        {
            return await GetActiveAsync<DropDownRecord>();
        }

        public async Task UpdateAsync(T entity)
        {
            await Repository.UpdateAsync(entity);
        }
    }
}
