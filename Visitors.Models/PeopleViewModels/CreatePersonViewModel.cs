﻿using System;
using System.ComponentModel.DataAnnotations;
using Visitors.Domain.Enums;

namespace Visitors.Models.PeopleViewModels
{
     public class CreatePersonViewModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string LastName { get; set; }
        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Phone { get; set; }
        [Display(Name = "Пол")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public Gender Gender { get; set; }
        [MaxLength(12, ErrorMessage = "Максимальная длина 12 символов")]
        [MinLength(12, ErrorMessage = "Минимальная длина 12 символов")]
        [Display(Name = "ИИН")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string IIN { get; set; }
 
        public CreatePersonViewModel()
        {
            Gender = Gender.NotSet;
        }
    }
}
