﻿using Visitors.Models.VisitsViewModels;

namespace Visitors.Models.PeopleViewModels
{
    public class PersonDetailViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string IIN { get; set; }
        public VisitViewModel[] Visits { get; set; }
    }
}
