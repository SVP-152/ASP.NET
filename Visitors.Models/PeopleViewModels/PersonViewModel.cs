﻿using System;

namespace Visitors.Models.PeopleViewModels
{
    public class PersonViewModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int TotalVisits { get; set; }
    }
}
