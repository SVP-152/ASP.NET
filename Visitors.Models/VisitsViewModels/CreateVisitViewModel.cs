﻿using System;

namespace Visitors.Models.VisitsViewModels
{
    public class CreateVisitViewModel
    {
        public Guid VisitorId{ get; set; }
        public Guid PersonId{ get; set; }
        public int Floor{ get; set; }
        public string Room{ get; set; }
    
    }
}
