﻿using Visitors.Models.VisitsViewModels;

namespace Visitors.Models.VisitorsViewModels
{
    public class VisitorDetailViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string IIN { get; set; }
        public string Address { get; set; }
        public VisitViewModel[] Visits { get; set; }
    }
}
